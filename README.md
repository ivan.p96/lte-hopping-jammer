# LTE hopping jammer

## Requirements

It requires the following libraries:

- USRP Hardware Driver (UHD)
- FFTW3

## Build

To build the jammer follow the following commands:
1.  `$ mkdir build`
2.  `$ cd build`
3.  `$ cmake ..`
4.  `$ make`

If uhd lib are in non standard paths, use something similar:

`$ cmake -DUHD_LIBRARIES=/usr/lib/x86_64-linux-gnu/libuhd.so.3.14.1 -DUHD_INCLUDE_DIRS=/usr/include ..`


## Usage

This version of the jammer is compatible with SDR with two chains, i.e., USRP B210. Both chains will be used for improving jamming capabilities. It creates a multiuhd device (with two channels) and will use both for transmitting "noisy" signal at the same frequency over time.

There are three modes, it can:
1. Generate a lte frame structure;
2. Use existing data that loads from files;
3. Generate random data.

Parameters that are common to the three modes:

- --freqs	list of frequencies where it will hop;
- --hoptime	number of millisecond before retuning radio;
- --gain	gain of the radio chains;
- --subdev	subdevices that will be added to the multiuhd device;
- --args	UHD style parameter for specifying which device to use;

### Examples for the three modes and specific paramters:

1. Generate a lte frame structure (structure only, random content):

`$ sudo ./build/jammer2chains --freqs <frequencies to jam> --gain <gain> --args "serial=<USRP serial>" --subdev "A:A A:B" --channels "0,1" --lte "<base channel multiplier>,<carriers to populate>, <regeneration time in half-subframe>" --hoptime "hopping time"`

LTE parameters are:
   - 4 : multiplier with respect to lte base frequency, i.e., with 4 it generates a 25PRB structure
   - 60 : number of contiguous carriers that it populates
   - 200 : number of half-subframes before performing microhop (within current lte channel, i.e., 200 means 100ms).

Example

`$ sudo ./build/jammer2chains --freqs 1820000000,2450000000 --gain 70 --args "serial=3094D6E" --subdev "A:A A:B" --channels "0,1" --lte "4,60,200" --hoptime 5000`



2. Use existing data that loads from files:

`$ jammer2chains --freqs 5785000000,5240000000 --gain 70 --args "type=b200" --subdev "A:A A:B" --channels "0,1" --hoptime 100 --file1 path/to/file1.dat --file2 /path/to/file2.dat --rate 20000000`

- Parameters --file1 and --file2 point to files that contains data encoded as shorts, little-endian. The two files must have the same length.
- Parameter --rate specificies the number of samples per second.

It would be better to use two different datafiles higly incorrelated (same length).

3. Generate random data: the jammer autonomously generates random samples

`$ jammer2chains --freqs 5745000000,5240000000,5280000000 --gain 70 --args "serial=3094D6E" --subdev "A:A A:B" --channels "0,1" --hoptime 240 --rate 20000000`

- Parameter --rate specificies the number of samples per second.
